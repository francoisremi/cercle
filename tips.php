<?php

include('skeleton/top.php');

?>

<div id="content">

<div class="text">
<h1>Quelques conseils pour utiliser un cercle</h1>
<ul>
<li>Vous pouvez naviguer entre les différents pages via le menu de gauche.</li>
<li>Si vous le souhaitez, vous pouvez indiquer votre nom en cliquant en haut à droite du pad.</li>
<li>Vous pouvez choisir de surligner ou non les contributions par auteurs en cliquant sur l'engrenage en haut à droite du pad.</li>
<li>Vous pouvez également vous connecter pour accéder aux pages privées.</li>
<li>Être connecté permet également de modifier l'arborescence du projet en faisant des cliques droit. Vous pourrez ainsi créer des pages ou des dossiers et gérer finement qui a accès à quoi.</li>
</ul>

<div class="error">
Attention, gardez à l'esprit que les pads ne sont pas strictement sécurisés. Ce site permet d'accéder facilement à leurs liens. Cependant, toute personne connaissant le lien peut les ouvrir par ailleurs. La sécurité est donc relative à la complexité des liens des pads, c'est à dire de leur identifiant visible dans votre barre d'url. Enfin, l'hébergeur des pads peut avoir accès à la liste de tous les pads créés sur son instance.
</div>
<br/><br/>
Pour information, les pads de ce site sont hébergés sur "<?php echo($PAD_ADDRESS); ?>".

</div>

</div>

</body>

<script type="text/javascript">make_footer()</script>

</html>
