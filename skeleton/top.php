<?php
session_start();
include('config.php');
include('quicktools.php');

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

?>
<!DOCTYPE html><html>
<head>
	<title><?php echo($WEBSITE_TITLE);?></title>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="style.css">
	<link rel="shortcut icon" type="image/png" href="icons/circle_background.svg"/>
</head>

<script src="js/footer.js"></script>

<!-- passage des infos sur les groupes au javascript -->

<script type="text/javascript">

<?php

if (isset($_SESSION['id'])){
    echo('SESSION_GROUPS = Array();');
    foreach($_SESSION['groups'] as $k => $g){
        echo('SESSION_GROUPS['.$k.'] = "'.$g.'";');
    }

    echo('SESSION_ID='.$_SESSION['id'].';');
}
else{
    echo('SESSION_GROUPS = Array();SESSION_GROUPS[1]="Publique";');
}
?>

SESSION_GROUPS_LEVEL = Array();
<?php
if (isset($_SESSION['id'])){

    foreach($_SESSION['groups_level'] as $k => $level){
        echo('SESSION_GROUPS_LEVEL['.$k.'] = "'.$level.'";');
    }
}
?>

LEVELS_ICONS_PATHS = Array();
<?php
foreach($LEVELS_ICONS_PATHS as $level => $icons_path){
        echo('LEVELS_ICONS_PATHS['.$level.'] = "'.$icons_path.'";');
    }
?>

LEVELS_NAMES = Array();
<?php
foreach($LEVELS_NAMES as $level => $name){
        echo('LEVELS_NAMES['.$level.'] = "'.$name.'";');
    }

echo('WEBSITE_TITLE="'.$WEBSITE_TITLE.'";');
echo('WEBSITE_ADDRESS="'.$WEBSITE_ADDRESS.'";');
echo('PAD_ADDRESS = "'.$PAD_ADDRESS.'"');
?>
</script>


<body>
<div id="left">
<a href="index.php" class="logo">
    <div id="logo" class="main_title">
        <?php if (isset($LOGO_PATH)){echo('<img src="'.$LOGO_PATH.'"/>');}?>
        <h1><?php echo($WEBSITE_TITLE);?></h1>
    </div>
</a>
<div id="user">
<?php
if (!isset($_SESSION['id'])){
echo('<a href="login.php">Se connecter</a>');
}
else{
echo($_SESSION['name'].' - <a href="setting.php"><img src="icons/setting.svg" style="width:1em"/></a> <a href="logout.php"><img src="icons/mono-logout.svg" class="logout" style="width:1em"/></a>');
}
?>
</div>

</div>
