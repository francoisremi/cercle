<?php
include('skeleton/top.php');
?>
<!-- Modal : renommer -->
<div id="modal" class="modal">
    <!-- Modal content -->
    <div class="modal-content">
        <span class="close">&times;</span>
        
        <!-- 	Renommer -->
        <form action="javascript:void(0);" id="rename_form" class="modal_form">
        <label for="rename_newname">Renommer:</label><br>
        <input type="text" id="rename_newname" name="rename_newname"><br>
        <input type="hidden" id="rename_path" name="rename_path">
        <input type="submit" value="OK" onclick="submit_rename_modal()">
        </form>
        
        <!-- 	changer de groupe -->
        <form action="javascript:void(0);" id="new_group_form" class="modal_form">
        <input type="hidden" id="new_group_path" name="new_group_path">
        <label for="group">Nouveau groupe: </label>
        <select name="new_group_id" id="new_group_id">
            <?php
            if (isset($_SESSION['id'])){
                foreach ($_SESSION['groups'] as $k => $g) {
                echo('<option value="'.$k.'" id="newGroup_select_id'.$k.'">'.$g.'</option>');
                }
            }
            ?>
        </select> <br>
        <input type="submit" value="OK" onclick="submit_new_group_modal()">
        </form>
        
        <!-- 	Créer une page -->
        <form action="javascript:void(0);" id="newpage_form" class="modal_form">
        <label for="newpage_name">Nom:</label><br>
        <input type="text" id="newpage_name" name="newpage_name" placeholder="titre"><br>
        <input type="hidden" id="newpage_path" name="newpage_path">
        <label for="group">Groupe:</label>
        <select name="newpage_group_id" id="newpage_group_id">
            <?php
            if (isset($_SESSION['id'])){
                foreach ($_SESSION['groups'] as $k => $g) {
                echo('<option value="'.$k.'" id="newGroup_select_id'.$k.'">'.$g.'</option>');
                }
            }
            ?>
        </select> <br>
        <input type="submit" value="OK" onclick="submit_newpage_modal()">
        </form>
        
        <!-- 	Importer un pdf -->
        <form action="admin/updateServer.php" method="post" id="newpdf_form" class="modal_form" enctype="multipart/form-data">
        <label for="newpdf_file">PDF à importer:</label><br>
        <input type="file" name="newpdf_file" id="newpdf_file"/><br/>
        <input type="hidden" id="newpdf_info" name="info" value="0">
        <input type="hidden" id="newpdf_path" name="path">
        <input type="hidden" id="updateServer" name="updateServer" value="newPdf">
        <label for="group">Groupe:</label>
        <select name="groupId" id="newpdf_group_id">
            <?php
            if (isset($_SESSION['id'])){
                foreach ($_SESSION['groups'] as $k => $g) {
                echo('<option value="'.$k.'" id="newGroup_select_id'.$k.'">'.$g.'</option>');
                }
            }
            ?>
        </select> <br>
        <input type="submit" value="OK"><br/>
        Taille limite du fichier : 5Mo.
        </form>
        
        <!-- 	Créer un dossier -->
        <form action="javascript:void(0);" id="newfolder_form" class="modal_form">
        <label for="newfolder_name">Nom:</label><br>
        <input type="text" id="newfolder_name" name="newfolder_name" placeholder="titre"><br>
        <input type="hidden" id="newfolder_path" name="newfolder_path">
        <label for="group">Groupe:</label>
        <select name="newfolder_group_id" id="newfolder_group_id">
            <?php
            if (isset($_SESSION['id'])){
                foreach ($_SESSION['groups'] as $k => $g) {
                echo('<option value="'.$k.'" id="newGroup_select_id'.$k.'">'.$g.'</option>');
                }
            }
            ?>
        </select> <br>
        <input type="submit" value="OK" onclick="submit_newfolder_modal()">
        </form>
        
        
        <!-- 	Supprimer -->
        <form action="javascript:void(0);" id="delete_form" class="modal_form">
        <input type="hidden" id="delete_path" name="delete_path">
        <div id="delete_confirm">Confirmer la suppression ?</div><br>
        <input type="submit" value="Oui" onclick="submit_delete_modal()">
        <input type="submit" value="Non" onclick="close_modal()">
        </form>
    </div>
    
</div>

</body>

<!-- 	Initialise la page -->
		<script src="js/init.js"></script>
	
<!-- 	Construit le menu -->
		<script src="js/construct.js"></script>

<!-- Répond aux clics sur le menu -->
	<script src="js/click.js"></script>
	
<!-- Actions modifiant le menu -->
	<script src="js/option.js"></script>
	
<!-- Met à jour le serveur -->
	<script src="js/update.js"></script>
	
<!-- Affiche les pages dans le pad -->
	<script src="js/pad.js"></script>

<!-- script modal -->
<script src="js/modal.js"></script>
</html>
