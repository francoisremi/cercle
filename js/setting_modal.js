// Get the modal
var modal = document.getElementById("modal");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// close modal and hide all forms
close_modal = function() {
    modal.style.display = "none";
    var forms = document.getElementsByClassName("modal_form");
    
    for (i = 0; i < forms.length; i++) {
    forms[i].style.display = "none";
    } 
}

// open modal
open_modal = function(type, target) {
    modal.style.display = "block";
    
    if (type == "quitGroup"){
        open_quitGroup_modal(target);
    }
    if (type == "changeLevel"){
        open_changeLevel_modal(target);
    }
    if (type == "renameGroup"){
        open_renameGroup_modal(target);
    }
}


// When the user clicks on <span> (x), close the modal
span.onclick = close_modal

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    close_modal();
  }
} 

open_quitGroup_modal = function(target) {
 document.getElementById("quitGroup_form").style.display = "block";
 document.getElementById("quitGroup_user_id").value = target.getAttribute("userId");
 document.getElementById("quitGroup_id").value = target.getAttribute("groupId");
 if (target.getAttribute("userId") != SESSION_ID){
     document.getElementById("quitGroup_confirm").innerHTML = 'Voulez-vous vraiment exclure <strong>'+target.getAttribute("userName")+'</strong> du groupe "'+target.getAttribute("groupName")+'" ?';
 }
 else{
    document.getElementById("quitGroup_confirm").innerHTML = 'Voulez-vous vraiment quitter le groupe "'+target.getAttribute("groupName")+'" ?';
 }
}

open_changeLevel_modal = function(target) {
 document.getElementById("changeLevel_form").style.display = "block";
 
 var user_id = target.getAttribute("userId");
 var group_id = target.getAttribute("groupId");
 var user_name = target.getAttribute("userName");
 var group_name = target.getAttribute("groupName");
 var user_level = target.getAttribute("userLevel");
document.getElementById("quitGroup_button").setAttribute("userId", user_id)
document.getElementById("quitGroup_button").setAttribute("groupId", group_id)
document.getElementById("quitGroup_button").setAttribute("userName", user_name)
document.getElementById("quitGroup_button").setAttribute("groupName", group_name)

 document.getElementById("changeLevel_description").innerHTML = 'L\'utilisateurice <strong>'+user_name+'</strong> fait partie du groupe "'+group_name+'" en tant que <strong>'+LEVELS_NAMES[user_level]+'</strong> <img class="level" src="'+LEVELS_ICONS_PATHS[user_level]+'"/>';
  document.getElementById("changeLevel_user_id").value = user_id;
  document.getElementById("changeLevel_group_id").value = group_id;
  
 document.getElementById("changeLevel_select_"+user_level).selected = true;
}

open_renameGroup_modal = function(target){
    document.getElementById("renameGroup_form").style.display = "block";
    document.getElementById("renameGroup_newname").value = target.getAttribute("groupName");
    document.getElementById("renameGroup_group_id").value = target.getAttribute("groupId");
}
