// Élément HTML du menu d'options
var 	option = document.createElement("div");
		option.id = "option";
		option.classList.add("hidden");
document.body.appendChild(option);

// Ouverture du menu
option.open = function ( element, x, y )
{
	this.target = element;
	this.classList.remove("hidden");
	this.style.left = (x - 20) + "px";
	this.style.top = (y - 10) + "px";
	
	// Affiche/ cache l'option "ouvrir dans un nouvel onglet" si l'élément est une page/ un dossier
	if 	((element.classList.contains("page_title") || element.classList.contains("pdf_title")) && option.newTab.classList.contains("hidden"))
    {
		option.newTab.classList.remove("hidden");
        option.linkCopy.classList.remove("hidden");
    }
	else if (element.classList.contains("folder_title") && !option.newTab.classList.contains("hidden"))
    {
		option.newTab.classList.add("hidden");
        option.linkCopy.classList.add("hidden");
    }
}

// Fermeture du menu (lorsqu'appelée ou lorsque la sourie sors de l'élément HTML)
option.close = option.onmouseleave = function ()
{
	this.classList.add("hidden");
}

// Ouvrir dans un nouvel onglet (seulement pour les pages)
option.newTab = document.createElement("div");
option.newTab.link = document.createElement("a");
option.newTab.link.target = "_blank";
option.newTab.link.innerHTML = "Ouvrir dans un nouvel onglet";

option.newTab.onclick = function() 
{
		this.link.href = pad.adress + option.target.id; 
};

option.newTab.appendChild(option.newTab.link);
option.appendChild(option.newTab);

// valeur du lien
option.linkValue = document.createElement("input");
option.linkValue.style.position = 'absolute';
option.linkValue.style.left = '-9999px';

option.linkCopy = document.createElement("div");
option.linkCopy.innerHTML = "Copier le lien";
option.linkCopy.onclick = function()
{ 
    option.linkValue.value = WEBSITE_ADDRESS+'#'+option.target.id;
     option.linkValue.select(); 
     document.execCommand("copy");
     option.close();
}

option.appendChild(option.linkValue);
option.appendChild(option.linkCopy);

// Renommer un élément
option.rename = document.createElement("div");
option.rename.innerHTML = "Renommer";

option.rename.onclick = function()
{ 
    open_modal("newName", option.target);
}

option.appendChild(option.rename);

// changer de groupe
option.newGroup = document.createElement("div");
option.newGroup.innerHTML = "Changer de groupe";

option.newGroup.onclick = function()
{ 
    open_modal("newGroup", option.target);
}

option.appendChild(option.newGroup);

// Déplacer
option.move = document.createElement("div");
option.move.innerHTML = "Déplacer";
option.move.active = false;

option.move.onclick = function()
{ 
	var elements = menu.getElementsByTagName("li");
	for (var i = 0; i < elements.length; i++)
	{
		elements[i].style.cursor = "s-resize";
	}
	
	option.move.active = true;
	option.close();
}

option.move.from = function (from)
{
	update("move", from.path, option.target.path);
	
	this.stop();
}

option.move.stop = function()
{
	var elements = menu.getElementsByTagName("li");
	for (var i = 0; i < elements.length; i++)
	{
		elements[i].style.cursor = "pointer";
	}
	
	option.move.active = false;
}

option.appendChild(option.move);

// Ajouter une page
option.newPage = document.createElement("div");
option.newPage.innerHTML = "Créer une page";

option.newPage.onclick = function()
{ 
    open_modal("newPage", option.target);
}

option.appendChild(option.newPage);

// Ajouter un fichier PDF
option.newPdf = document.createElement("div");
option.newPdf.innerHTML = "Importer un PDF";

option.newPdf.onclick = function()
{ 
    open_modal("newPdf", option.target);
}

option.appendChild(option.newPdf);

// Ajouter un dossier
option.newFolder = document.createElement("div");
option.newFolder.innerHTML = "Créer un dossier";

option.newFolder.onclick = function()
{ 
    open_modal("newFolder", option.target);
}

option.appendChild(option.newFolder);



// Supprime
option.deleted = document.createElement("div");
option.deleted.innerHTML = "Supprimer";

option.deleted.onclick = function()
{ 
    open_modal("delete", option.target);
}

option.appendChild(option.deleted);
