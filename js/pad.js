// Iframe du pad
var 	pad = document.createElement("iframe");
		pad.adress = PAD_ADDRESS;
		pad.id = "pad";
		pad.current = location.hash.substr(1);
		pad.def = null;
        
		
// Ouvre une nouvelle page dans le pad
pad.open = function (element)
{
    // si l'élément est un pad
    if (element.classList.contains('page_title')){
        this.src = this.adress + element.id;
    }
    
    // si l'élément est un pdf
    else if (element.classList.contains('pdf_title')){
        this.src = 'storage/'+element.id+'.pdf';
    }
    
    if (this.current != null)
    {
        this.current.classList.remove("selected");
    }
    this.current = element;
    this.current.classList.add("selected");
    
    window.location.hash = element.id;

}

// Démarre le pad
pad.start = function()
{
    console.log(this.current)
	// Intègre l'iframe
	document.body.appendChild(pad);

	// Si aucune page n'est visée dans le hash de l'URL
    hash = location.hash.substr(1)
	if (hash == '')
	{
		// Vise la page par défaut
		this.current = this.def;
	}
	else
    {
        // Vise la page du hash
        this.current = document.getElementsByClassName("id_"+hash)[0];
    }

	// Ouvre la page
	pad.open(this.current);

	// Déplie les dossiers correspondants dans le menu
	var child = this.current;
	for (var i = 0; i < this.current.path.length-1; i++)
	{
		var parent = child.parentNode.previousSibling; 
		parent.classList.add("open");
		child = parent;
	}
}
