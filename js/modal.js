
// Get the modal
var modal = document.getElementById("modal");
//var newpage_modal = document.getElementById("newpage_modal");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];


// close modal and hide all forms
close_modal = function() {
    modal.style.display = "none";
    var forms = document.getElementsByClassName("modal_form");
    
    for (i = 0; i < forms.length; i++) {
    forms[i].style.display = "none";
    } 
}

// open modal
open_modal = function(type, target) {
    modal.style.display = "block";
    
    if (type == "newName"){
        open_rename_modal(target)
    }
    if (type == "newPage"){
        open_newpage_modal(target)
    }
    if (type == "newFolder"){
        open_newfolder_modal(target)
    }
    if (type == "delete"){
        open_delete_modal(target)
    }
    if (type == "newGroup"){
        open_new_group_modal(target)
    }
    if (type == "newPdf"){
        open_newpdf_modal(target)
    }
}

// open rename modal
open_rename_modal = function(target) {
    document.getElementById("rename_form").style.display = "block";
    document.getElementById("rename_newname").value = target.title;
    document.getElementById("rename_path").value = target.path;
}

// submit rename modal
submit_rename_modal = function(target) {
    var path = document.getElementById("rename_path").value;
    var name = document.getElementById("rename_newname").value;
    console.log(path);
    console.log(name);
    if (name != null)
	{
		update("newName", name, path);
	}
    close_modal();
}

// open newpage modal
open_newpage_modal = function(target) {
    document.getElementById("newpage_form").style.display = "block";
    document.getElementById("newpage_path").value = target.path;
}

// submit newpage modal
submit_newpage_modal = function(target) {
    var name = document.getElementById("newpage_name").value;
    var path = document.getElementById("newpage_path").value;
    var group_id = document.getElementById("newpage_group_id").value;
    if (name != null)
	{
		update("newPage", name, path, group_id);
	}
    close_modal();
    document.getElementById("newpage_name").value = "";
}

// open newpdf modal
open_newpdf_modal = function(target) {
    document.getElementById("newpdf_form").style.display = "block";
    document.getElementById("newpdf_path").value = target.path;
}

// submit newpdf modal
submit_newpdf_modal = function(target) {
    var files = document.getElementById("newpdf_file").files;
    var path = document.getElementById("newpdf_path").value;
    var group_id = document.getElementById("newpdf_group_id").value;
    
    
    if (files.length == 1)
	{
        var file = files[0];
        var name = file.name.substr(0, file.name.lastIndexOf('.'));
        update("newPdf", name, path, group_id, file=file);
	}
    close_modal();
    clearInputFile(document.getElementById("newpdf_file"));
}

// open newfolder modal
open_newfolder_modal = function(target) {
    document.getElementById("newfolder_form").style.display = "block";
    document.getElementById("newfolder_path").value = target.path;
}

// submit newfolder modal
submit_newfolder_modal = function(target) {
    var name = document.getElementById("newfolder_name").value;
    var path = document.getElementById("newfolder_path").value;
    var group_id = document.getElementById("newfolder_group_id").value;
    if (name != null)
	{
		update("newFolder", name, path, group_id);
	}
    close_modal();
    document.getElementById("newfolder_name").value = "";
}

// open delete modal
open_delete_modal = function(target) {
    document.getElementById("delete_form").style.display = "block";
    document.getElementById("delete_confirm").innerHTML = "Voulez-vous vraiment supprimer " + (target.classList.contains("page_title") ? "la page" : "le dossier") + " <" + target.innerHTML + "> ?";
    document.getElementById("delete_path").value = target.path;
}

// submit delete modal
submit_delete_modal = function(target) {
    var path = document.getElementById("delete_path").value;
    update("delete", "", path);
    close_modal();
}

// open new group modal
open_new_group_modal = function(target) {
    document.getElementById("new_group_form").style.display = "block";
    document.getElementById("new_group_path").value = target.path;
    document.getElementById("newGroup_select_id"+target.group).selected = true;
}

// submit set group modal
submit_new_group_modal = function(target) {
    var path = document.getElementById("new_group_path").value;
    var group_id = document.getElementById("new_group_id").value;
    update("newGroup", "", path, group_id);
    close_modal();
}




// When the user clicks on <span> (x), close the modal
span.onclick = close_modal

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    close_modal();
  }
} 

function clearInputFile(f){
    if(f.value){
        try{
            f.value = ''; //for IE11, latest Chrome/Firefox/Opera...
        }catch(err){ }
        if(f.value){ //for IE5 ~ IE10
            var form = document.createElement('form'),
                parentNode = f.parentNode, ref = f.nextSibling;
            form.appendChild(f);
            form.reset();
            parentNode.insertBefore(f,ref);
        }
    }
}
