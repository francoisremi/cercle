<?php
session_start();
/* ----------------------
Mise à jour du serveur
---------------------- */


function random_str(
    $length,
    $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
) {
    $str = '';
    $max = mb_strlen($keyspace, '8bit') - 1;
    if ($max < 1) {
        throw new Exception('$keyspace must be at least two characters long');
    }
    for ($i = 0; $i < $length; ++$i) {
        $str .= $keyspace[random_int(0, $max)];
    }
    return $str;
}

// L'emplacement du fichier contenant le menu
$menuFile = "../server/menu.json";

// L'emplacement du fichier contenant les éléments supprimés
$deletedFile = "../server/deleted.json";

// L'emplacement du storage
$storageFolder = "../storage/";

// hash dans le cas du header
$hash = "";

if (isset($_SESSION['id']))
{
    // Lorsque reçoit une demande de mise à jour du serveur
    if (isset($_POST['updateServer']))
    {
        // Recupère l'objet principal contenu dans le fichier
        $content = json_decode(file_get_contents($menuFile));
        
        // Recupère le type de la modification, spécifié par la demande
        $type = htmlentities($_POST['updateServer'], ENT_QUOTES);
        
        // Recupère le contenu de la modification, spécifié par la demande
        $info = htmlentities($_POST['info'], ENT_QUOTES);
        
        // Recupère la position de l'élément du menu à partir duquel faire la modification, spécifiée par la demande
        $path = explode('-',htmlentities($_POST['path'], ENT_QUOTES));
        
        // Recupère l'ID de groupe
        $group_id = htmlentities($_POST['groupId'], ENT_QUOTES);
        
        // Récupère le fichier si il y a
        if ($type == "newPdf" && isset($_FILES['newpdf_file']) && $_FILES["newpdf_file"]["error"] == 0){
            $allowed = array("pdf" => "application/pdf");
            $info = pathinfo($_FILES["newpdf_file"]["name"], PATHINFO_FILENAME);
            $filetype = $_FILES["newpdf_file"]["type"];
            $filesize = $_FILES["newpdf_file"]["size"];
            
        
            // Verify file extension
            $ext = pathinfo($_FILES["newpdf_file"]["name"], PATHINFO_EXTENSION);
            if(!array_key_exists($ext, $allowed)) die("Error: Please select a valid file format.");
        
            // Verify file size - 5MB maximum
            $maxsize = 5 * 1024 * 1024;
            if($filesize > $maxsize) die("Error: File size is larger than the allowed limit.");
        
            // Verify MYME type of the file
            if(!in_array($filetype, $allowed)) die("Error: Please select a valid MIME file format.");
        
        }

        // Renomme
        if ($type == "newName")
            $content = newName($content, $info, $path, $_SESSION['groups_level']);
            
        else if ($type == "newGroup")
            $content = newGroup($content, $info, $path, $group_id, $_SESSION['groups_level']);
        
        // Ajoute
        else if ($type == "newPage" || $type == "newFolder" || $type == "newPdf")
            $content = addElement($content, $info, $path, $type, $group_id, $_SESSION['groups_level']);
        
        // Déplace
        else if ($type == "move")
            $content = moveElement($content, $info, $path, $_SESSION['groups_level']);
        
        // Supprime
        else if ($type == "delete")
            $content = deleteElement($content, $path, $_SESSION['groups_level']);
        
        
        // Met à jour l'objet principal du fichier
        file_put_contents ($menuFile, json_encode($content));
        
        // si c'est un upload, retour sur index.php
        if ($type == "newPdf")
            header("location: ../index.php#".$hash);
        
        // Envoie au client le contenu du tableau de l'objet principal
        echo json_encode($content->content);
    }
}
/* ----------------------
Créer un élément
---------------------- */

function createElement($title, $type, $group_id)
{
	// Crée un nouvel élément
	$element = new stdClass;
	
	// Donne un id de groupe à l'élément
    $element->group = $group_id;
	
	// Si la demande concerne l'ajout d'une page
	if ($type == "newPage")
	{
        // Donne au nouvel élément un ID aléatoire
        $element->id = random_str(20);
	
        // Donne au nouvel élément le titre celui indiquée par la demande
        $element->title = $title;
	
		// Donne à l'élément le type "page"
		$element->type = "page";
	}
	
	// Si la demande concerne l'ajout d'un dossier
	else if ($type == "newFolder")
	{
        // Donne au nouvel élément un ID aléatoire
        $element->id = random_str(20);
	
        // Donne au nouvel élément le titre celui indiquée par la demande
        $element->title = $title;
	
		// Donne à l'élément le type "folder"
		$element->type = "folder";
		
		// Donne à l'élément un tableau qui contiendra ses sous-éléments
		$element->content = [];
	}
	
	// Si la demande concerne l'ajout d'un pdf 
	else if ($type == "newPdf")
	{
        // Donne au nouvel élément un ID aléatoire
        $element->id = random_str(40);
	
        // Donne au nouvel élément le titre du fichier
        $element->title = $title;
        
        // Donne à l'élément le type "pdf"
        $element->type = "pdf";
	}
	
	return $element;
}

/* ----------------------
Récupérer un élément
---------------------- */

function getElement($content, $path)
{
	$element = $content;
	for ($i = 0; $i < count($path); $i++)
	{
		$element = $element->content[$path[$i]];
	}
	return $element;
}

/* ----------------------
Récupérer le parent d'un élément
---------------------- */

function getParent($content, $path)
{
	$element = $content;
	for ($i = 0; $i < count($path) -1; $i++)
	{
		$element = $element->content[$path[$i]];
	}
	return $element;
}

/* ----------------------
Renomer
---------------------- */

function newName($content, $info, $path, $groups_level)
{
	// Recupère l'élément
	$element = getElement($content, $path);
	
	// on vérifie si on a les droits :
	if ($groups_level[$element->group] >= 1)
	{
        // on vérifie si le nom est correct
        if ($info != ''){
            // Donne à l'élément le titre indiqué
            $element->title = $info;
        }
    }
    // Renvoie l'objet principal modifié
    return $content;
}

/* ----------------------
Changer de groupe
---------------------- */

function newGroup($content, $info, $path, $group_id, $groups_level)
{
	// Recupère l'élément
	$element = getElement($content, $path);
	
	// on vérifie si on a les droits :
	if (($groups_level[$element->group] >= 1) && ($groups_level[$group_id] >= 1))
	{
        // Donne à l'élément le titre indiqué
        $element->group = $group_id;
    }
	
	// Renvoie l'objet principal modifié
	return $content;
}


/* ----------------------
Insérer
---------------------- */

function insertElement($content, $path, $element, $groups_level)
{
	// Recupère l'élément cliqué (en fonction duquel le nouvel élément doit être ajouté)
	$syb = getElement($content, $path);
	
	// Si l'élément cliqué est un dossier
	if ($syb->type == "folder")
	{
        // on vérifie si on a les droits :
        if ($groups_level[$syb->group] >= 1)
        {
            // Ajoute le nouvel élément au début du dossier
            array_unshift($syb->content, $element);
		}
		else
		{
		return 0;
		}
	}
	
	// Si l'élément cliqué est une page
	else if ($syb->type == "page" || $syb->type == "pdf")
	{
		// Recupère le dossier parent de la page cliquée
		$parent = getParent($content, $path);
        
        // on vérifie si on a les droits :
        if ($groups_level[$parent->group] >= 1)
        {
            // Ajoute le nouvel élément dans le dossier parent, après la page cliquée
            array_splice( $parent->content, $path[ count($path) -1 ] +1 , 0, array ( $element ) );
        }
        else
		{
		return 0;
		}
	}
	
	return $content;
}


/* ----------------------
Ajouter un nouvel élément
---------------------- */

function addElement($content, $info, $path, $type, $group_id, $groups_level)
{
    global $storageFolder;
    global $hash;
    
    // Vérifie si on a les droits sur le groupe cible
    if ($groups_level[$group_id] >= 1)
    {
        // on vérifie si le nom est correct
        if ($info != ''){
            // Crée un nouvel élément
            $newElement = createElement($info, $type, $group_id);
            
            $newContent =  insertElement($content, $path, $newElement, $groups_level);
            
            if ($type == "newPdf")
            {
                // upload with random name
                move_uploaded_file($_FILES["newpdf_file"]["tmp_name"], $storageFolder . $newElement->id . ".pdf");
                $hash = $newElement->id;
            }
            
            // si l'insertion est un échec
            if (is_int($newContent))
            {
                return $newContent;
            }
        }
    }
    
    return $content;
}

/* ----------------------
Déplacer
---------------------- */

function moveElement($content, $pathTo, $path, $groups_level)
{

	$pathTo=explode('-',$pathTo);

	// Vérifie que l'élément à déplacer n'est pas un parent du dossier où on cherche à le déplacer
	$continue = false;
	
	if ( count($pathTo) >= count($path) )
	{
		
		for ($i = 0; $i < count($path) AND $continue == false; $i++)
		{
			if ( $pathTo[$i] != $path[$i] ) 
				$continue = true;
		}
	}
	else
	{
		$continue = true;
	}

	if ($continue)
	{
		// Récupère l'élément à déplacer
		$element = getElement($content, $path);
		
		// On vérifie que l'on a les droits sur l'élément
		if ($groups_level[$element->group] >= 1)
        {
            // Récupère le parent de l'élément à déplacer
            $parent = getParent($content, $path);

            // Fait une copie de l'élément à déplacer
            $elementCopy = clone $element;

            // Insère la copie de l'élément à déplacer à l'endroit indiqué
            $newContent = insertElement($content, $pathTo, $elementCopy, $groups_level);
            
            if ($newContent != 0)
            {
                $content = $newContent;
                
                // Indique que l'original de l'élément à déplacé doit être supprimé
                $element->type = "removed";
                
                // Supprime l'original de l'élément à déplacer
                for ($i = 0; $i < count($parent->content); $i++)
                {
                    if ($parent->content[$i]->type == "removed")
                        array_splice( $parent->content, $i , 1 );
                }
                
            }

            
		}
	}
	
	return $content;
}

/* ----------------------
Supprimer
---------------------- */
function deleteElement($content, $path, $groups_level)
{
    global $deletedFile;
    global $storageFolder;
    
	// Récupère l'élément à supprimer
	$element = getElement($content, $path);
    
    // Vérifie si on a les droits
    if ($groups_level[$element->group]>=1){
        // Récupère son parent
        $parent = getParent($content, $path);
        
        // ajoute la date pour la sauvegarde
        $element->date = date('Y:m:d:H:i:s');

        // Met l'élément dans le fichier des éléments supprimés
        file_put_contents ($deletedFile, json_encode( $element ), FILE_APPEND );
        
        // si c'est un pdf 
        if ($element->type = "pdf")
        {
            $filepath = $storageFolder.$element->id.'.pdf';
            // on supprime le fichier associé si il existe
            if (file_exists($filepath)) {
                unlink($filepath);
            }
        }
        
        // Supprime l'élément de son parent
        array_splice( $parent->content, $path[ count($path) -1 ] , 1 );
	}
	
	// Renvoie l'objet principal modifié
	return $content;
}



?>
