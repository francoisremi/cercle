<?php

include('PHPMailer/mail.php');

function random_str(
    $length,
    $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
) {
    $str = '';
    $max = mb_strlen($keyspace, '8bit') - 1;
    if ($max < 1) {
        throw new Exception('$keyspace must be at least two characters long');
    }
    for ($i = 0; $i < $length; ++$i) {
        $str .= $keyspace[random_int(0, $max)];
    }
    return $str;
}

function newGroup($db, $name) {
    global $MYSQL_TABLE_GROUPS;
    
    // on test, est-ce que ce nom est pris ?
    $check_group = getGroupByName($db, $name);
    if ($check_group != 0) // oui
    {
        return("Ce nom de groupe '".$name."' est déjà pris. Merci d'en choisir un autre.");
    }
    
    $sql = "INSERT INTO `".$MYSQL_TABLE_GROUPS."` (`id`, `name`) VALUES (NULL, '".$name."')";

    if (mysqli_query($db,$sql)) {
    } else {
    return("Error: " . $sql . "<br>" . $db->error);
    }
    
    $group_id = mysqli_insert_id($db);
    
    $_SESSION['groups'][$group_id] = $name;
    $_SESSION['groups_level'][$group_id] = 2;
    
    $addUsergroups_r = addUsergroups($db, $_SESSION['id'], $group_id, 2);
    
    if ($addUsergroups_r != 1){
        return($addUsergroups_r);
    }
    
    return(1);
}

function getGroupByName($db, $name){
    global $MYSQL_TABLE_GROUPS;
    
    // on cherche le groupe qui porte ce nom
    $sql = 'SELECT id, name FROM '.$MYSQL_TABLE_GROUPS.' WHERE name="'.$name.'"';
    $result = mysqli_query($db,$sql);
    $count = mysqli_num_rows($result);
    
    // si il existe, on le retourne
    if ($count == 1)
    {
        $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
        return($row);
    }
    else // si il n'existe pas, on retourne 0
    {
        return(0);
    }
}

function renameGroup($db, $group_id, $newname){
    global $MYSQL_TABLE_GROUPS;
    
    // test le nom est-il correct ?
    if (strlen($newname)>0)
    {
        // test l'user a-t-il les droits ?
        if ($_SESSION['groups_level'][$group_id] == 2)
        {
            
            $sql = "UPDATE `".$MYSQL_TABLE_GROUPS."` SET `name` = '".$newname."' WHERE `id` = '".$group_id."'";
            if (mysqli_query($db,$sql)){
                $_SESSION['groups'][$group_id] = $newname;
            }
            else {
                return("Error: " . $sql . "<br>" . $db->error);
            }
        }
        else
        {
            return('Vous n\'avez pas les droits !');
        }
    }
    else
    {
        return('Le groupe doit bien avoir un nom !');
    }
    return(1);
}

function addUsergroups($db, $user_id, $group_id, $level) {
    global $MYSQL_TABLE_USERGROUPS;
    
    // test est-ce que l'on appartient soit même au groupe ?
    if (in_array($group_id, array_keys($_SESSION['groups'])))
    {
        // test est-ce que l'on a les droits pour ajouter au groupe avec le level demandé ?
        if ($_SESSION['groups_level'][$group_id] >= $level)
        {
            // test est-il déjà dans le groupe ?
    
            $sql_testgroup = 'SELECT id, level FROM '.$MYSQL_TABLE_USERGROUPS.' WHERE user_id="'.$user_id.'" AND group_id="'.$group_id.'"';
            
            $result_test_group = mysqli_query($db,$sql_testgroup);
            $count = mysqli_num_rows($result_test_group);
            
            if ($count == 0){
            
                $sql = "INSERT INTO `".$MYSQL_TABLE_USERGROUPS."` (`id`, `user_id`, `group_id`, `level`) VALUES (NULL, '".$user_id."', '".$group_id."', '".$level."')";

                if (mysqli_query($db,$sql)) {
                }
                else {
                return "Error: " . $sql . "<br>" . $db->error;
                }
            }
            else
            {
                return('Cet utilisateur appartient déjà à ce groupe !');
            }
        }
        else
        {
            return('Vous devez être du niveau requis pour cette opération');
        }
    }
    else
    {
        return('Vous devez appartenir au groupe pour cette opération');
    }
}

function changeLevel($db, $user_id, $group_id, $level){
    global $MYSQL_TABLE_USERGROUPS;
    
    // test est-ce qu'il a les droits ?
    if ($_SESSION['groups_level'][$group_id] >= $level)
    {
        // test est-ce qu'il change bien le niveau d'autrui ?
        if ($_SESSION['id'] != $user_id){
        
            $sql = "UPDATE `".$MYSQL_TABLE_USERGROUPS."` SET `level` = '".$level."' WHERE `user_id` = ".$user_id." AND `group_id` = ".$group_id;
            if (mysqli_query($db,$sql)) {
            }
            else {
            return "Error: " . $sql . "<br>" . $db->error;
            }
        }
        else
        {
            return('Vous ne pouvez pas changer votre propre niveau');
        }
    }
    else
    {
        return('Vous n\'avez pas les droits');
    }
    
    return(1);
}

function addUsergroupsWithLogin($db, $mail, $group_id, $level) {
    global $MYSQL_TABLE_USERS;
    
    $sql = 'SELECT id FROM '.$MYSQL_TABLE_USERS.' WHERE mail="'.$mail.'"';
    $result = mysqli_query($db,$sql);
    $count = mysqli_num_rows($result);
    
    if ($count == 1){
        $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
        
        return(addUsergroups($db, $row['id'], $group_id, $level));
    }
    else if ($count == 0){
        return('Login inconnu');
    }
    else {
        return('Erreur de mail');
    }
    
}

function quitGroup($db, $user_id, $group_id){
    global $MYSQL_TABLE_USERGROUPS;
    
    if (($user_id != $_SESSION['id']) && ($_SESSION['groups_level'][$group_id] < 2)){
        return('Vous ne pouvez pas faire quitter une autre utilisateurice si vous n\'êtes pas admin du groupe');
    }
    
    if (($user_id == $_SESSION['id']) && ($_SESSION['groups_level'][$group_id] == 2)){
    return('Vous ne pouvez pas quitter un groupe où vous êtes admin !');
    }
    
    if($group_id==1){
    return('Vous ne pouvez pas quitter ce groupe.');
    }
    
    $sql_quitgroup = 'DELETE FROM `'.$MYSQL_TABLE_USERGROUPS.'` WHERE `user_id` = '.$user_id.' AND `group_id` = '.$group_id;
    
    $result_quitgroup = mysqli_query($db,$sql_quitgroup);
    
    if ($user_id == $_SESSION['id']){
        unset($_SESSION['groups'][$group_id]);
        unset($_SESSION['groups_level'][$group_id]);
    }
    return(1);
}

function inviteUser($db, $mail){
    global $WEBSITE_TITLE;
    global $WEBSITE_ADDRESS;
    global $MYSQL_TABLE_INVITETOKENS;

    // vérifie si ce mail n'est pas déjà dans les users
    if (countMail($db, $mail) == 0){
        // vérifie si mail n'est pas déjà dans les token
        if (countMailToken($db, $mail)==0){
            
            $token = random_str(80);
            
            $link = $WEBSITE_ADDRESS.'/signup.php?token='.$token;
            
            $sql = "INSERT INTO `".$MYSQL_TABLE_INVITETOKENS."` (`id`, `token`, `mail`) VALUES (NULL, '".$token."', '".$mail."')";
            $r = mysqli_query($db,$sql);
            
            $subject = "Invitation à rejoindre ".$WEBSITE_TITLE." !";
            $body = 'Tu as été invité.e à rejoindre le cercle intitulé "'.$WEBSITE_TITLE.'" !<br/>Il te suffit de cliquer sur ce lien pour procéder à l\'inscription :<br/>';
            $body = $body . '<a href="' . $link . '">' . $link . '</a>';
            return(send_mail($mail, $subject, $body));
        }
        else
        {
            return('mail déjà invité');
        }
    }
    else
    {
        return('mail déjà inscrit.');
    }
}

function countMailToken($db, $mail)
{
    global $MYSQL_TABLE_INVITETOKENS;
    
    $sql = "SELECT id FROM ".$MYSQL_TABLE_INVITETOKENS." WHERE mail='".$mail."'";
    $result = mysqli_query($db,$sql);
    $count = mysqli_num_rows($result);
    
    return($count);
}

function checkInviteToken($db, $token)
{
    global $MYSQL_TABLE_INVITETOKENS;
    
    $sql = "SELECT id, mail FROM ".$MYSQL_TABLE_INVITETOKENS." WHERE token='".$token."'";
    $result = mysqli_query($db,$sql);
    $count = mysqli_num_rows($result);
    
    if ($count == 1){
        return(1);
    }
    else{
        return(0);
    }
}

function getTokenMail($db, $token){
    global $MYSQL_TABLE_INVITETOKENS;
    
    $sql = "SELECT id, mail FROM ".$MYSQL_TABLE_INVITETOKENS." WHERE token='".$token."'";
    $result = mysqli_query($db,$sql);
    
    $count = mysqli_num_rows($result);
    
    if ($count==1)
    {
        $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
        return($row['mail']);
    }
    else
    {
        return("");
    }
    
}

function countMail($db, $mail)
{
    global $MYSQL_TABLE_USERS;
    
    $sql = "SELECT id FROM ".$MYSQL_TABLE_USERS." WHERE mail='".$mail."'";
    $result = mysqli_query($db,$sql);
    $count = mysqli_num_rows($result);
    
    return($count);
}

function newUser($db, $token, $pass_md5, $name){
    global $MYSQL_TABLE_USERS;
    global $MYSQL_TABLE_USERGROUPS;
    global $MYSQL_TABLE_INVITETOKENS;
    
    $mail = getTokenMail($db, $token);
    
    if (strlen($mail)>0)
    {
        if (countMail($db,$mail) == 0){
            $sql = "INSERT INTO `".$MYSQL_TABLE_USERS."` (`id`, `mail`, `pass_md5`, `name`) VALUES (NULL, '".$mail."', '".$pass_md5."','".$name."')";
            $r = mysqli_query($db,$sql);
            
            $user_id = mysqli_insert_id($db);
            
            $sql = "INSERT INTO `".$MYSQL_TABLE_USERGROUPS."` (`id`, `user_id`, `group_id`, `level`) VALUES (NULL, '".$user_id."', '1', '0')";
            $r = mysqli_query($db,$sql);
            
            $sql_delete_token = "DELETE FROM `".$MYSQL_TABLE_INVITETOKENS."` WHERE `token` = '".$token."'";
            $r = mysqli_query($db,$sql_delete_token);
            
        }
        else{
            return('mail déjà utilisé');
        }

        return(1);
    }
    else
    {
        return('erreur token mail: '.$mail);
    }
}

?>
