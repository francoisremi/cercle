<?php
session_start();
/* ----------------------
Mise à jour du client
---------------------- */ 
// L'emplacement du fichier contenant le menu
$file = "server/menu.json";

function check_groups($j, $groups){
    $idx = -1;
    $id_to_remove = array();
    foreach ($j->content as &$sub_j) {
        $idx = $idx + 1;
        if (!in_array($sub_j->group, $groups)){
            array_push($id_to_remove, $idx);
        }
        else{
            if (isset($sub_j->content)){
                $sub_j = check_groups($sub_j, $groups);
            }
        }
    }
    
    if (count($id_to_remove)>0){
        arsort($id_to_remove);
        foreach ($id_to_remove as &$i) {
            //unset($j->content[$i]);
            $j->content[$i] = "hidden";
        }
    }
    return $j;
}

// Lorsque reçoit une demande de mise à jour du client
if (isset($_POST['updateClient']))
{
	// Récupère le contenu du fichier contenant le menu
	$content = file_get_contents($file);
	
	// Si le fichier n'existe pas encore ou contient une table vide
	//if (!$content OR count( json_decode($content)->content ) == 0 )
	//{
	//	// Crée l'objet principal
	//	$main = new stdClass;
		
		// Donne à l'objet principal un tableau qui contiendra ses éléments
	//	$main->content = array();
	
		// Créé un premier élément
	//	$element = new stdClass;
	
		// Donne au nouvel élément le titre celui indiquée par la demande
	//	$element->title = "Home";
	
		// Donne au nouvel élément un ID aléatoire
	//	$element->id = rand(10000000, 99999999);
	
		// Donne à l'élément le type "page"
	//	$element->type = "page";
		
		// Met le nouvel élément dans le tableau de l'objet principal
	//	array_push($main->content, $element);
		
		// Convertit l'objet principal en JSON
	//	$content = json_encode($main);
		
		// Crée le fichier en y incluant le JSON
	//	file_put_contents ($file, $content);
	//}
	
	$content_json = json_decode($content);
	
	if (isset($_SESSION['id']))
    {
        $groups_ids = array_keys($_SESSION['groups']);
    }
    else
    {
    $groups_ids = array(1);
    }
    
    
    $content_json = check_groups($content_json, $groups_ids);
    
	// Envoie au client le contenu du tableau de l'objet principal
	echo json_encode($content_json->content);
}

?>
