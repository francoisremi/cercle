<?php
function display_level_icon($level){
    global $LEVELS_ICONS_PATHS;
    global $LEVELS_NAMES;
    
    return('<img class="level" src="'.$LEVELS_ICONS_PATHS[$level].'" title="'.$LEVELS_NAMES[$level].'"/>');
}
?>
