<?php

include('skeleton/top.php');

if (!isset($_SESSION['id'])){
    header('Location: login.php');
}


include("admin/updateDBB.php");

if($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST['newGroup_name'])){
        $name = htmlentities($_POST['newGroup_name'], ENT_QUOTES);
        $error = newGroup($db, $name);
    }
    if (isset($_POST['quitGroup_id'])){
        $user_id = mysqli_real_escape_string($db,$_POST['quitGroup_user_id']);
        $group_id = mysqli_real_escape_string($db,$_POST['quitGroup_id']);
        $error = quitGroup($db, $user_id, $group_id);
    }
    if (isset($_POST['newUsergroup_mail'])){
        $mail = strtolower(mysqli_real_escape_string($db,$_POST['newUsergroup_mail']));
        $group_id = mysqli_real_escape_string($db,$_POST['newUsergroup_group_id']);
        $error = addUsergroupsWithLogin($db, $mail, $group_id, 0);
    }
    if (isset($_POST['inviteUser_mail'])){
        $mail = mysqli_real_escape_string($db,$_POST["inviteUser_mail"]);
        if (!filter_var($mail, FILTER_VALIDATE_EMAIL)) {
            $error = "Invalid email format";
        }
        else{
            $error = inviteUser($db, $mail);
        }
        if ($error == 1){
            $message = 'Mail d\'invitation envoyé à '.$mail.'.';
        }
    }
    if (isset($_POST['changeLevel_select'])){
        $user_id = mysqli_real_escape_string($db,$_POST['changeLevel_user_id']);
        $group_id = mysqli_real_escape_string($db,$_POST['changeLevel_group_id']);
        $level = mysqli_real_escape_string($db,$_POST['changeLevel_select']);
        
        $error = changeLevel($db, $user_id, $group_id, $level);
    }
    if (isset($_POST['renameGroup_newname'])){
        $newname = htmlentities($_POST['renameGroup_newname'], ENT_QUOTES);
        $group_id = mysqli_real_escape_string($db,$_POST['renameGroup_group_id']);
        
        $error = renameGroup($db, $group_id, $newname);
    }
}

?>

<div id="modal" class="modal">
    <!-- Modal content -->
    <div class="modal-content">
        <span class="close">&times;</span>
        
        <!-- 	Changer de niveau -->
        <form action="setting.php" id="changeLevel_form" class="modal_form" method="post">
        <div id="changeLevel_description"></div>
        <input type="hidden" id="changeLevel_user_id" name="changeLevel_user_id">
        <input type="hidden" id="changeLevel_group_id" name="changeLevel_group_id">
        <label for="changeLevel_select">Changer de niveau : </label>
        <select name="changeLevel_select" id="changeLevel_select">
            <?php
            foreach($LEVELS_NAMES as $level => $name){
            echo('<option value="'.$level.'" id="changeLevel_select_'.$level.'">'.$name.'</option>');
            }
            ?>
        </select><br/>
        
        <input type="submit" value="OK" onclick="changeLevel_modal()"><br/><br/>
        <div id="quitGroup_button" onclick="close_modal();open_modal('quitGroup', this)">Exclure du groupe</div>
        </form>
        
        <!-- 	Supprimer -->
        <form action="setting.php" id="quitGroup_form" class="modal_form" method="post">
        <input type="hidden" id="quitGroup_user_id" name="quitGroup_user_id">
        <input type="hidden" id="quitGroup_id" name="quitGroup_id">
        <div id="quitGroup_confirm">Voulez-vous vraiment quitter ce groupe ?</div><br>
        <input type="submit" value="Oui">
        </form>
        
        <!-- 	Renommer -->
        <form action="setting.php" id="renameGroup_form" class="modal_form" method="post">
        <label for="renameGroup_newname">Renommer:</label><br>
        <input type="text" id="renameGroup_newname" name="renameGroup_newname"><br>
        <input type="hidden" id="renameGroup_group_id" name="renameGroup_group_id">
        <input type="submit" value="OK">
        </form>
    </div>
    
</div>


<div id="content">
<?php
if (isset($error)){
if ($error != 1){
echo('<div class="error">'.$error.'</div>');
}
}
if (isset($message)){
echo('<div class="success-box">'.$message.'</div>');
}
?>
 <h2>Utilisateurices</h2>
 <p>Seul les utilisateurices qui appartiennent aux même groupes que vous sont visibles.</p>
  <table>
  <tr>
    <th>mail</th>
    <th>Groupes</th>
  </tr>
  <?php
    $sql = 'SELECT user_id, group_id, level FROM '.$MYSQL_TABLE_USERGROUPS.' WHERE group_id IN (';
    
    foreach($_SESSION['groups'] as $k => $g){
        $sql = $sql . '"' . $k . '",';
    }
    $sql = substr_replace($sql ,"", -1) . ")";
    
    $usergroups_result = mysqli_query($db,$sql);
    
    $count = mysqli_num_rows($usergroups_result);
        
    $sql = 'SELECT id, mail FROM '.$MYSQL_TABLE_USERS.' WHERE id IN (';
    $usergroups = Array();
    while ($row_usergroups = mysqli_fetch_array($usergroups_result))
    {
        $sql = $sql . '"' . $row_usergroups['user_id'] . '",';
        
        if (!isset($usergroups[$row_usergroups['user_id']])){
            $usergroups[$row_usergroups['user_id']] = Array();
        }
        
        $usergroups[$row_usergroups['user_id']][$row_usergroups['group_id']] = $row_usergroups['level'];

        
    }
    $sql = substr_replace($sql ,"", -1) . ")";
    
    $users_result = mysqli_query($db,$sql);
    
    while ($row_users = mysqli_fetch_array($users_result))
    {
  ?>
  <tr>
    <td><?php echo($row_users['mail']);?></td>
    <td><?php
    $str_groups = "";
    foreach($usergroups[$row_users['id']] as $id_group=>$level)
    {
        $str_groups = $str_groups . $_SESSION['groups'][$id_group];
        
        // ajout de l'icone de changement de niveau
        $str_groups = $str_groups . '<div ';
        
        // si il ne s'agit pas de l'utilisateur connecté
        if ($row_users['id'] != $_SESSION['id'])
        {
            // ajouter ce qu'il faut pour le bouton de changement de niveau
            $str_groups = $str_groups . 'class="button" onclick="open_modal(\'changeLevel\', this)" userName="'.$row_users['mail'].'" userId="' .$row_users['id']. '" userLevel="'.$level.'" groupId="'.$id_group.'" groupName="'.$_SESSION['groups'][$id_group].'"';
        }
        else
        {
            // ajouter le bouton inactif
            $str_groups = $str_groups . 'class="button-inactive"';
        }
        
        // fin du bouton de changement de niveau
        $str_groups = $str_groups . ' >'.display_level_icon($level).'</div>';
        
        $str_groups = $str_groups . ' - ';
    }
    $str_groups = substr($str_groups ,0, -2);
    echo($str_groups);
    ?></td>
  </tr>
    <?php
    }
    ?>
</table>
<h2>Vos groupes :</h2>
  <table>
  <tr>
    <th>id</th>
    <th></th>
    <th>nom</th>
    <th></th>
    <th></th>
    <th></th>
  </tr>
  <?php

    foreach ($_SESSION['groups'] as $k => $g)
    {
  ?>
  <tr>
    <td><?php echo($k);?></td>
    <td><?php echo(display_level_icon($_SESSION['groups_level'][$k])); ?></td>
    <td><?php echo($g);?></td>
    <td>
    <?php
    if($_SESSION['groups_level'][$k] == 2){
    ?>
    <img src="icons/spanner.svg" class="icons button" groupId="<?php echo($k);?>" groupName="<?php echo($g);?>" onclick="open_modal('renameGroup',this)"/>
    <?php
    }
    ?>
    </td>
    <td><div class="quitButton button" userId="<?php echo($_SESSION['id']); ?>" groupId="<?php echo($k);?>" groupName="<?php echo($g);?>" onclick="open_modal('quitGroup', this)">-</div></td>
    
  </tr>
  <?php
  }
  ?>
</table> 

<h3>Créer un groupe</h3>
<form action="setting.php" method="post">
<label for="newGroup_name">Nom du groupe :</label><input type="text" name="newGroup_name" value="">
 <input type="submit" name="newGroup_submit" value="Créer">
</form><br/>

<h3>Ajouter une utilisateurice à un groupe</h3>
<form action="setting.php" method="post">
<label for="newUsergroup_mail">mail :</label>
<input type="text" name="newUsergroup_mail" value=""><br/>
<label for="newUsergroup_group_id">Groupe :</label>
<select name="newUsergroup_group_id" id="newUsergroup_group_id">
    <?php
    if (isset($_SESSION['mail'])){
        foreach ($_SESSION['groups'] as $k => $g) {
        echo('<option value="'.$k.'" id="newGroup_select_id'.$k.'">'.$g.'</option>');
        }
    }
    ?>
</select><br/>

<input type="submit" name="newUsergroup_submit" value="Ajouter">
</form>



<h3>Inviter une nouvelle utilisateurice</h3>
<form action="setting.php" method="post">
<label for="inviteUser_mail">Mail : </label><input type="text" name="inviteUser_mail" value="">
 <input type="submit" name="inviteUser_submit" value="Créer">
</form><br/>

</div>
</body>

<!-- script modal -->
<script src="js/setting_modal.js"></script>

<script type="text/javascript">make_footer()</script>

</html>
