<?php

include('skeleton/top.php');

if ((!isset($_GET['token'])) && (!isset($_POST['name']))){
    header("location: index.php");
}

include("admin/updateDBB.php");

if (isset($_GET['token'])){
    $token = mysqli_real_escape_string($db,$_GET['token']);
}

if (isset($_POST['token'])){
    $token = mysqli_real_escape_string($db,$_POST['token']);
}

if (checkInviteToken($db, $token) == 1){

if (isset($_POST['token'])){
    $name = htmlentities($_POST['name']);
    $pass = mysqli_real_escape_string($db,$_POST['pass']);
    $pass_confirm = mysqli_real_escape_string($db,$_POST['passconfirm']);
    
    if ($name == "") {
            $error = "Nom invalide";
    }
    else{
        if (strlen($pass) >= 8){
            if ($pass == $pass_confirm){
                $error = newUser($db, $token, md5($pass),$name);
                
                if ($error == 1){
                    header("location: login.php?signup=1");
                }
            }
            else {
                $error = 'mots de passes différents.';
            }
        }
        else
        {
            $error = 'Le mot de passe doit faire au minimum 8 caractères.';
        }
    }
}
?>

<div id="content">
    Inscription<br />
    <form action="signup.php" method="post">
    <label for="name">Nom :</label><input type="text" name="name" value="<?php if (isset($_POST['name'])) echo htmlentities(trim($_POST['name'])); ?>"><br />
    <label for="pass">Mot de passe :</label><input type="password" name="pass" value=""><br />
    <label for="passconfirm">Confirmer le mot de passe :</label><input type="password" name="passconfirm" value=""><br />
    <input type="hidden" name="token" value="<?php echo($token); ?>">
    <label for="connexion"></label><input type="submit" name="Inscription" value="Inscription">
    </form>
    <?php
    if (isset($error)){echo('<br /><br />'.$error);}
    ?>
</div>

<?php
}
else{
?>
<div id="content">
L'url d'invitation n'existe pas ou est périmé.
</div>
<?php

}?>

</body>

<script type="text/javascript">make_footer()</script>

</html>
