<?php

include('skeleton/top.php');

?>

<div id="content">

<div class="text">
<h1>À propos</h1>

Le cercle est un logiciel de structuration de pads qui est un fork du <a href="https://git.laquadrature.net/la-quadrature-du-net/carre/">carré</a>, logiciel utilisé par La Quadrature du net. Vous pouvez contribuer au cercle sur <a href="https://framagit.org/francoisremi/cercle">le dépôt git</a>.
</div>

</div>

</body>

<script type="text/javascript">make_footer()</script>

</html>
