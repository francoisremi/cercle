<?php

include('skeleton/top.php');

if($_SERVER["REQUEST_METHOD"] == "POST") {
    
    // username and password sent from form 
    
    $mail = strtolower(mysqli_real_escape_string($db,$_POST['mail']));
    $pass = mysqli_real_escape_string($db,$_POST['pass']); 
    
    $sql = "SELECT id, name FROM ".$MYSQL_TABLE_USERS." WHERE mail = '" . $mail . "' AND pass_md5 = '" . md5($pass) . "'";
    
    $result = mysqli_query($db,$sql);
    
    $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
    $count = mysqli_num_rows($result);
        
    // If result matched $myusername and $mypassword, table row must be 1 row
    
    if($count == 1) {
        $_SESSION['id'] = $row['id'];
        $_SESSION['mail'] = $mail;
        $_SESSION['name'] = $row['name'];
        
        $sql = "SELECT user_id, group_id, level FROM ".$MYSQL_TABLE_USERGROUPS." WHERE user_id = '" . $row['id'] . "'";
        $usergroups_result = mysqli_query($db,$sql);
        
        $sql_groups = "SELECT id, name FROM ".$MYSQL_TABLE_GROUPS." WHERE id IN (";
        
        $_SESSION['groups_level'] = Array();
        
        while ($row_usergroups = mysqli_fetch_array($usergroups_result))
        {
            $sql_groups = $sql_groups . "'".$row_usergroups['group_id']."',";
            $_SESSION['groups_level'][$row_usergroups['group_id']] = $row_usergroups['level'];

        }
        $sql_groups = substr_replace($sql_groups ,"", -1) . ")";
        
        $groups_result = mysqli_query($db,$sql_groups);
        
        $_SESSION['groups'] = Array();
        while ($row_groups = mysqli_fetch_array($groups_result))
        {
            $_SESSION['groups'][$row_groups['id']] = $row_groups['name'];
        }
        
        header("location: index.php");
    }
    else
    {
        $error = "Votre identifiant ou votre mot de passe est invalide.";
    }
}

?>

<div id="content">
    <?php
    if (isset($_GET['signup'])){
    ?>
    <div class='box-success'>Inscription réussie ! Vous pouvez dès à présent vous connecter</div>
    <?php
    }
    ?>

    Connexion à l'espace utilisateurices :<br />
    <form action="login.php" method="post">
    <label for="mail">Mail :</label><input type="text" name="mail" value="<?php if (isset($_POST['mail'])) echo htmlentities(trim($_POST['mail'])); ?>"><br />
    <label for="pass">Mot de passe :</label><input type="password" name="pass" value="<?php if (isset($_POST['pass'])) echo htmlentities(trim($_POST['pass'])); ?>"><br />
    <label for="connexion"></label><input type="submit" name="connexion" value="Connexion">
    </form>
    <?php
    if (isset($error))
    {
        echo('<br /><br /><div class="error">'.$error.'</div>');
    }
    else
    {
        echo('<br/><br/>');
    }
    ?>
    
    <p>
    Il est nécessaire de recevoir un mail d'invitation pour s'inscrire sur <?php echo($WEBSITE_TITLE); ?>.
    </p>
</div>

</body>

<script type="text/javascript">make_footer()</script>

</html>
