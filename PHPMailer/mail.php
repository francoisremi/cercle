<?php

//include('../config.php');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'src/Exception.php';
require 'src/PHPMailer.php';
require 'src/SMTP.php';


function send_mail($adress, $subject, $body){
  global $MAIL_HOST;
  global $MAIL_PORT;
  global $MAIL_USERNAME;
  global $MAIL_PASS;
  global $MAIL_NAME;
  
  $mail             = new PHPMailer();
  $mail->CharSet = 'UTF-8';
  $mail->IsSMTP();
  $mail->SMTPAuth   = true;
  $mail->SMTPOptions = array('ssl' => array('verify_peer' => false,'verify_peer_name' => false,'allow_self_signed' => true)); // ignorer l'erreur de certificat.
  $mail->Host       = $MAIL_HOST;  
  $mail->Port       = $MAIL_PORT;
  $mail->Username   = $MAIL_USERNAME;
  $mail->Password   = $MAIL_PASS;        
  $mail->From       = $MAIL_USERNAME; //adresse d’envoi correspondant au login entré précédemment
  $mail->FromName   = $MAIL_NAME; // nom qui sera affiché
  $mail->Subject    = $subject; // sujet
  $mail->WordWrap   = 50; // nombre de caractères pour le retour à la ligne automatique
  $mail->MsgHTML($body); 
  $mail->AddReplyTo($MAIL_USERNAME,$MAIL_NAME);
  //$mail->AddAttachment("./examples/images/phpmailer.gif");// pièce jointe si besoin
  $mail->AddAddress($adress);
  $mail->IsHTML(true); // envoyer au format html, passer a false si en mode texte 
  if(!$mail->Send()) {
    return("Mailer Error: " . $mail->ErrorInfo);
  } else {
    return(1);
  } 
  
}

?>
